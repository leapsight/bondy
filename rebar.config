{minimum_otp_vsn, "R24"}.

{erl_opts, [
    % warn_export_all,
    % warn_missing_spec,
    debug_info,
    ewarn_export_all,
    strict_validation,
    warn_bif_clash,
    warn_deprecated_function,
    warn_export_vars,
    warn_exported_vars,
    warn_format,
    warn_obsolete_guard,
    warn_shadow_vars,
    % warn_untyped_record,
    warn_unused_function,
    warn_unused_import,
    warn_unused_record,
    warn_unused_vars,
    warnings_as_errors
 ]}.

{deps, [
    %% -------------------------------------------------------------------------
    %% Crypto
    %% -------------------------------------------------------------------------
    {jose,
        {git, "https://github.com/potatosalad/erlang-jose.git", {tag, "1.11.2"}}
    },
    {pbkdf2,
        {git, "https://github.com/leapsight-oss/erlang-pbkdf2.git", {branch, "master"}}
    },
    {stringprep, "1.0.27"},
    {enacl, "1.2.1"},
    %% -------------------------------------------------------------------------
    %% Web Server|client
    %% -------------------------------------------------------------------------
    %% Used to implement WebSockets, API Gateway, HTTP Admin API
    %% and /metrics endpoint
    {cowboy, "2.9.0"},
    %% Used by API Gateway to implement forward action to
    %% downstream HTTP services
    hackney,
    backoff,
    %% -------------------------------------------------------------------------
    %% Utils
    %% -------------------------------------------------------------------------
    {uuid, "2.0.2", {pkg, uuid_erl}},
    %% -------------------------------------------------------------------------
    %% Concurrency|Load|Traffic Management
    %% -------------------------------------------------------------------------
    %% Currently used to implement a load regulated pool of workers
    sidejob,
    %% A hashed set of ets tables and an implementation of a queue that we
    %% currently used for bondy_rpc_promise.
    {tuplespace,
        {git, "https://gitlab.com/leapsight/tuplespace.git", {branch, "master"}}
    },
    {jobs, "0.10.0"},
    %% -------------------------------------------------------------------------
    %% Instrumentation/Debugging
    %% -------------------------------------------------------------------------
    bear,
    {observer_cli, "1.7.1"},
    %% Exposes metrics to Promethues
    {prometheus, "4.8.1"},
    {prometheus_cowboy, "0.1.8"},
    {redbug, "2.0.7"},
    {telemetry, "1.0.0"},
    %% -------------------------------------------------------------------------
    %% WAMP message encoding/decoding
    %% -------------------------------------------------------------------------
    {wamp,
        {git, "https://gitlab.com/leapsight/wamp.git", {tag, "0.9.4"}}
    },
    %% -------------------------------------------------------------------------
    %% Core Deps
    %% -------------------------------------------------------------------------
    %% The embedded database using Partisan, Plumtree and dvvsets.
    %% Stores data in ets and leveldb, also performs active anti-entropy
    %% exchanges to keep all nodes in sync.
    {plum_db,
        {git, "https://gitlab.com/leapsight/plum_db.git", {tag, "1.0.0-beta.10"}}
    },

    %% A partial implementation of an Adaptive Radix Trie.
    %% We use it to store the procedure and topic tries.
    {art,
        {git, "https://gitlab.com/leapsight/art.git", {branch, "develop"}}
    },
    %% A mustache-like library used by API Gateway and Broker Bridge
    %% specifications
    {mops,
        {git, "https://gitlab.com/leapsight/mops.git", {branch, "master"}}
    },
    %% Leapsight utility library implementing relational algebra
    {leap,
        {git, "https://gitlab.com/leapsight/leap.git", {branch, "master"}}
    },
    %% Leapsight utility library
    {utils,
        {git, "https://gitlab.com/leapsight/utils.git", {tag, "1.3.4"}}
    }
]}.



%% =============================================================================
%% RELX
%% =============================================================================


{relx, [
    {release, {bondy, "1.0.0-beta.21"},[
        %% Erlang/OTP
        crypto,
        inets,
        kernel,
        runtime_tools,
        sasl,
        stdlib,
        tools,
        compiler, %% required by sidejob
        %% Crypto
        enacl,
        jose,
        pbkdf2,
        stringprep,
        %% Web Server|client
        cowboy,
        hackney,
        %% Utils
        uuid,
        %% Concurrency|Load|Traffic Management
        jobs,
        sidejob,
        %% Instrumentation/Debugging
        bear,
        observer_cli,
        prometheus,
        prometheus_cowboy,
        redbug,
        telemetry,
        %% Serialization formats
        jsone,
        msgpack,
        %% 1st-party deps
        art,
        leap,
        mops,
        utils,
        wamp,
        {tuplespace, load},
        {plum_db, load},
        %% Bondy Apps
        bondy,
        bondy_broker_bridge
    ]},

    {overlay, [
        {template, "priv/hooks/pre_start", "bin/hooks/pre_start"},
        {copy, "priv/hooks/status","bin/hooks/status"},
        %% copy scripts required by extended_start_script_hooks
        {copy,
            "priv/tools/validate-config", "bin/validate-config"
        },
        {copy,
            "priv/tools/replace-env-vars","bin/replace-env-vars"
        },
        {template,
            "priv/tools/bondy_remote_console", "bin/bondy_remote_console"
        },
        {template,
            "priv/tools/db-repair.escript", "bin/db-repair.escript"
        }
    ]},

    {overlay_vars, "config/prod/vars.generated"},
    {extended_start_script, true},
    {extended_start_script_hooks, [
        {pre_start, [
            {custom, "hooks/pre_start"},
            {custom, "hooks/pre_start_cuttlefish"}
        ]},
        {status, [
            {custom, "hooks/status"}
        ]}
    ]}
]}.

{pre_hooks, [
    %% We generate the vars.generated we set in overlay_vars and use in by
    %% relx overlay template command to replace the mustache variables
    %% e.g. {{platform_data_dir}}
    {release, "make genvars"}
]}.


%% =============================================================================
%% REBAR
%% =============================================================================


{project_plugins, [
    pc,
    rebar3_hex,
    rebar3_ex_doc,
    rebar3_proper,
    rebar3_hank,
    {rebar3_scuttler,
        {git, "https://github.com/leapsight/rebar3_scuttler",
            {branch, "master"}}}
]}.

{shell, [
    {config, "config/dev/advanced.config"},
    {apps, [bondy, bondy_broker_bridge]}
]}.

{profiles, [
    {prod, [
        {relx, [
            % {mode, prod},
            {debug_info, strip},
            {dev_mode, false},
            {include_erts, true},
            {include_src, false},
            {system_libs, true},

            %% This file is generated by the pre_hook
            {overlay_vars, "config/prod/vars.generated"},
            {overlay, [
                {mkdir, "{{platform_data_dir}}"},
                {mkdir, "{{platform_log_dir}}"},
                {mkdir, "{{platform_etc_dir}}"},
                %% For some reason rebar and/or scuttler breaks if we do not
                %% provide a file, so we put an empty conf file that will be
                %% replaced by the user and if not it will take all default
                %% values defined by the schema.
                {template,
                    "config/prod/bondy.conf",
                    "{{platform_etc_dir}}/bondy.conf"
                },
                {template,
                    "config/prod/vm.args.template",
                    "releases/{{release_version}}/vm.args.template"
                },
                {template,
                    "config/prod/sys.config.template",
                    "releases/{{release_version}}/sys.config.template"
                }
            ]}
        ]}
    ]},
    {dev, [
        {relx, [
            % {mode, dev},
            {debug_info, keep},
            {dev_mode, true},
            {include_erts, false},
            {include_src, true},

            {overlay_vars, "config/dev/vars.config"},
            {overlay, [
                {mkdir, "{{platform_data_dir}}"},
                {mkdir, "{{platform_log_dir}}"},
                {mkdir, "{{platform_etc_dir}}"},
                {copy,
                    "config/dev/bondy.conf",
                    "{{platform_etc_dir}}/bondy.conf"
                },
                {template,
                    "config/ssl",
                    "{{platform_etc_dir}}/ssl"
                },
                {template,
                    "config/dev/vm.args.template",
                    "releases/{{release_version}}/vm.args.template"
                },
                {template,
                    "config/dev/sys.config.template",
                    "releases/{{release_version}}/sys.config.template"
                }
            ]}
        ]}
    ]},
    {node1, [
        {relx, [
            % {mode, prod},
            {debug_info, strip},
            {dev_mode, false},
            {include_erts, true},
            {include_src, false},
            {system_libs, true},

            {overlay_vars, "config/test/node_1_vars.config"},
            {overlay, [
                {mkdir, "{{platform_data_dir}}"},
                {mkdir, "{{platform_log_dir}}"},
                {mkdir, "{{platform_etc_dir}}"},
                {mkdir, "{{platform_etc_dir}}/ssl"},
                {template,
                    "config/test/node_1_bondy.conf",
                    "{{platform_etc_dir}}/bondy.conf"
                },
                {copy,
                    "config/ssl/cacert.pem",
                    "{{platform_etc_dir}}/ssl/cacert.pem"
                },
                {copy,
                    "config/ssl/cert.pem",
                    "{{platform_etc_dir}}/ssl/cert.pem"
                },
                {copy,
                    "config/ssl/key.pem",
                    "{{platform_etc_dir}}/ssl/key.pem"
                },
                {template,
                    "config/test/node_1_vm.args.template",
                    "releases/{{release_version}}/vm.args.template"
                },
                {template,
                    "config/test/sys.config.template",
                    "releases/{{release_version}}/sys.config.template"
                },
                {copy,
                    "examples/config/security_config.json",
                    "{{platform_etc_dir}}/security_config.json"
                }
            ]}
        ]}
    ]},
    {node2, [
        {relx, [
            % {mode, prod},
            {debug_info, strip},
            {dev_mode, false},
            {include_erts, true},
            {include_src, false},
            {system_libs, true},

            {overlay_vars, "config/test/node_2_vars.config"},
            {overlay, [
                {mkdir, "{{platform_data_dir}}"},
                {mkdir, "{{platform_log_dir}}"},
                {mkdir, "{{platform_etc_dir}}"},
                {template,
                    "config/test/node_2_bondy.conf",
                    "{{platform_etc_dir}}/bondy.conf"
                },
                {template,
                    "config/ssl",
                    "{{platform_etc_dir}}/ssl"
                },
                {template,
                    "config/test/node_2_vm.args.template",
                    "releases/{{release_version}}/vm.args.template"
                },
                {template,
                    "config/test/sys.config.template",
                    "releases/{{release_version}}/sys.config.template"
                },
                {copy,
                    "examples/config/security_config.json",
                    "{{platform_etc_dir}}/security_config.json"
                }
            ]}
        ]}
    ]},
    {node3, [
        {relx, [
            % {mode, prod},
            {debug_info, strip},
            {dev_mode, false},
            {include_erts, true},
            {include_src, false},
            {system_libs, true},

            {overlay_vars, "config/test/node_3_vars.config"},
            {overlay, [
                {mkdir, "{{platform_data_dir}}"},
                {mkdir, "{{platform_log_dir}}"},
                {mkdir, "{{platform_etc_dir}}"},
                {template,
                    "config/test/node_3_bondy.conf",
                    "{{platform_etc_dir}}/bondy.conf"
                },
                {template,
                    "config/ssl",
                    "{{platform_etc_dir}}/ssl"
                },
                {template,
                    "config/test/node_3_vm.args.template",
                    "releases/{{release_version}}/vm.args.template"
                },
                {template,
                    "config/test/sys.config.template",
                    "releases/{{release_version}}/sys.config.template"
                },
                {copy,
                    "examples/config/security_config.json",
                    "{{platform_etc_dir}}/security_config.json"
                }
            ]}
        ]}
    ]},
    {edge1, [
        {relx, [
            % {mode, prod},
            {debug_info, strip},
            {dev_mode, false},
            {include_erts, true},
            {include_src, false},
            {system_libs, true},

            {overlay_vars, "config/test/edge_1_vars.config"},
            {overlay, [
                {mkdir, "{{platform_data_dir}}"},
                {mkdir, "{{platform_log_dir}}"},
                {mkdir, "{{platform_etc_dir}}"},
                {template,
                    "config/test/edge_1_bondy.conf",
                    "{{platform_etc_dir}}/bondy.conf"
                },
                {template,
                    "config/test/edge_1_vm.args.template",
                    "releases/{{release_version}}/vm.args.template"
                },
                {template,
                    "config/test/sys.config.template",
                    "releases/{{release_version}}/sys.config.template"
                }
            ]}
        ]}
    ]},
    {bridge, [
        {relx, [
            % {mode, dev},
            {debug_info, keep},
            {dev_mode, true},
            {include_erts, false},
            {include_src, true},

            {vm_args, "config/bridge/vm.args"},
            {overlay_vars, "config/bridge/vars.config"},
            {overlay,
                [{mkdir, "{{platform_data_dir}}"},
                {mkdir, "{{platform_log_dir}}"},
                {mkdir, "{{platform_etc_dir}}"},
                {template,
                    "config/bridge/bondy.conf",
                    "{{platform_etc_dir}}/bondy.conf"
                },
                {copy,
                    "config/bridge/advanced.config",
                    "{{platform_etc_dir}}/advanced.config"
                },
                {copy,
                    "examples/config/security_config.json",
                    "{{platform_etc_dir}}/security_config.json"
                },
                {copy,
                    "examples/config/broker_bridge_config.json",
                    "{{platform_etc_dir}}/broker_bridge_config.json"
                }
            ]}
        ]}
    ]},
    {lint,  [
        {plugins, [
            rebar3_lint
        ]}
    ]},
    {test, [
        {deps, [meck, proper]},
        {erl_opts, [debug_info, export_all, nowarn_export_all]},
        {relx, [
            % {mode, dev},
            {debug_info, keep},
            {dev_mode, true},
            {include_erts, false},
            {include_src, true},

            {overlay_vars, "config/test/node_1_vars.config"},
            {overlay, [
                {mkdir, "{{platform_data_dir}}"},
                {mkdir, "{{platform_log_dir}}"},
                {mkdir, "{{platform_etc_dir}}"},
                {template,
                    "config/test/node_1_bondy.conf",
                    "{{platform_etc_dir}}/bondy.conf"
                },
                {template,
                    "config/test/node_1_vm.args.template",
                    "releases/{{release_version}}/vm.args.template"
                },
                {template,
                    "config/test/sys.config.template",
                    "releases/{{release_version}}/sys.config.template"
                },
                {copy,
                    "examples/config/security_config.json",
                    "{{platform_etc_dir}}/security_config.json"
                }
            ]}
        ]}
    ]}
]}.



%% =============================================================================
%% DOCS
%% =============================================================================



{ex_doc, [
    {name, "Bondy"},
    {source_url, <<"https://gitlab.com/leapsight/bondy">>},
    {homepage_url, "http://bondy"},
    {extra_section, "Pages"},

    %% Paths from root dir
    {extras, [
        <<"README.md">>,
        <<"LICENSE">>,
        <<"doc/guides/introduction/wamp.md">>,
        <<"CHANGELOG.md">>
    ]},
    {groups_for_extras, #{
        <<"Introduction">> => [
            <<"doc/guides/introduction/wamp.md">>
        ],
        <<"Tutorials">> => [],
        <<"How-to Guides">> => [],
        <<"Deployment">> => [],
        <<"Technical Reference">> => []
    }},
    {groups_for_modules, [
        {<<"Main">>, [
            bondy,
            bondy_app,
            bondy_config,
            bondy_consistent_hashing,
            bondy_error,
            bondy_data_validators,
            bondy_ref,
            bondy_sup,
            bondy_utils,
            bondy_wamp_utils
        ]},
        {<<"Multi-tenancy">>, [
            bondy_realm,
            bondy_security,
            bondy_sensitive
        ]},
        {<<"Authentication">>, [
            bondy_auth,
            bondy_auth_anonymous,
            bondy_auth_oauth2,
            bondy_auth_password,
            bondy_auth_ticket,
            bondy_auth_trust,
            bondy_auth_wamp_cra,
            bondy_auth_wamp_cryptosign,
            bondy_auth_wamp_scram,
            bondy_oauth2,
            bondy_password,
            bondy_password_cra,
            bondy_password_scram,
            bondy_ticket
        ]},
        {<<"Authorization">>, [
            bondy_cidr,
            bondy_rbac,
            bondy_rbac_group,
            bondy_rbac_source,
            bondy_rbac_user
        ]},
        {<<"Router">>, [
            bondy_broker,
            bondy_context,
            bondy_dealer,
            bondy_registry,
            bondy_registry_entry,
            bondy_retained_message,
            bondy_retained_message_manager,
            bondy_router,
            bondy_router_worker,
            bondy_rpc_load_balancer,
            bondy_rpc_promise,
            bondy_rpc_promise,
            bondy_subscriber,
            bondy_subscribers_sup,
            bondy_wamp_callback,
            bondy_wamp_protocol
        ]},
        {<<"Sessions">>, [
            bondy_session,
            bondy_session_manager,
            bondy_session_manager_sup
        ]},
        {<<"Listeners">>, [
            bondy_ranch_listener,
            bondy_retry,
            bondy_wamp_tcp,
            bondy_wamp_tcp_connection_handler,
            bondy_wamp_ws_connection_handler
        ]},
        {<<"Clustering & Edge">>, [
            bondy_peer_service,
            bondy_edge,
            bondy_edge_exchanges_sup,
            bondy_edge_session,
            bondy_edge_uplink_client,
            bondy_edge_uplink_client_sup,
            bondy_edge_uplink_server,
            bondy_peer_discovery_agent,
            bondy_peer_discovery_dns_agent,
            bondy_peer_discovery_static_agent,
            bondy_router_relay
        ]},
        {<<"Events & Telemetry">>, [
            bondy_alarm_handler,
            bondy_event_handler_watcher,
            bondy_event_handler_watcher_sup,
            bondy_event_logger,
            bondy_event_wamp_publisher,
            bondy_event_manager,
            bondy_prometheus,
            bondy_prometheus_collector,
            bondy_prometheus_cowboy_collector,
            bondy_wamp_event_manager,
            bondy_telemetry
        ]},
        {<<"WAMP API">>, [
            bondy_backup_wamp_api,
            bondy_http_gateway_wamp_api,
            bondy_oauth2_wamp_api,
            bondy_rbac_group_wamp_api,
            bondy_rbac_source_wamp_api,
            bondy_rbac_user_wamp_api,
            bondy_rbac_wamp_api,
            bondy_realm_wamp_api,
            bondy_session_wamp_api,
            bondy_telemetry_wamp_api,
            bondy_ticket_wamp_api,
            bondy_wamp_api,
            bondy_wamp_meta_api
        ]},
        {<<"HTTP GATEWAY">>, [
            bondy_admin_ping_http_handler,
            bondy_admin_ready_http_handler,
            bondy_http_gateway,
            bondy_http_gateway_api_spec,
            bondy_http_gateway_rest_handler,
            bondy_http_gateway_utils,
            bondy_oauth2_client,
            bondy_oauth2_resource_owner,
            bondy_oauth2_rest_handler
        ]},
        {<<"Other">>, [
            bondy_backup
        ]}
    ]},
    {api_reference, true},
    {main, <<"bondy">>}
]}.



%% =============================================================================
%% PLUGIN SPECIFIC CONFIG
%% =============================================================================



{hex, [
    {doc, #{provider => ex_doc}}
]}.

{scuttler, [
    {etc_dir, "etc"},
    {conf_file, "etc/bondy.conf"},
    {schemas, [
        {
            vm_args,
            "releases/{{release_version}}/generated/vm.generated.args"
        },
        {
            "{{deps_dir}}/eleveldb/priv",
            "releases/{{release_version}}/schema/",
            "releases/{{release_version}}/config/generated/user_defined.config"
        },
        {
            "{{deps_dir}}/plum_db/priv",
            "releases/{{release_version}}/schema/",
            "releases/{{release_version}}/config/generated/user_defined.config"
        },
        {
            auto_discover,
            "releases/{{release_version}}/schema",
            "releases/{{release_version}}/generated/user_defined.config"
        }
    ]},
    % Specifies where you'd like rebar3_scuttler to generate
    % the pre start hook to. This is intended to be then added
    % to the extended_start_script_hooks/pre_start relx entry list
    % for it to be invoked prior to the release start
    % This script will take care of processing `.schema` and `.conf`
    % files in order to output `.config` files that you will be able
    % to include from your own.
    {pre_start_hook, "bin/hooks/pre_start_cuttlefish"}
]}.



%% =============================================================================
%% TESTING
%% =============================================================================


{xref_checks, [undefined_function_calls]}.
{cover_enabled, true}.
{cover_opts, [verbose]}.
{ct_opts, [
    % {sys_config, "config/test/advanced.config"}
]}.



%% =============================================================================
%% CODE QUALITY
%% =============================================================================



{dialyzer, [
    {get_warnings, true},
    {plt_apps, top_level_deps}, % top_level_deps | all_deps
    %% {plt_extra_apps, [utils, wamp, leap]},
    % {plt_location, local}, % local | "/my/file/name"
    % {plt_prefix, "rebar3"},
    {base_plt_apps, [erts, stdlib, kernel, ssl, crypto, inets, os_mon]},
    % {base_plt_location, global}, % global | "/my/file/name"
    % {base_plt_prefix, "rebar3"},
    {warnings, [
        error_handling,
        no_behaviours,
        no_contracts,
        no_fail_call,
        no_fun_app,
        no_improper_lists,
        no_match,
        no_missing_calls,
        no_opaque,
        no_return,
        no_undefined_callbacks,
        no_unused,
        race_conditions,
        % underspecs,
        % overspecs,
        % specdiffs,
        unknown,
        unmatched_returns
    ]}
]}.

{elvis,
 [#{
        dirs => ["apps/bondy/src"],
        filter => "*.erl",
        rules => [
            %% {elvis_style, line_length,
            %%  #{ignore => [],
            %%    limit => 80,
            %%    skip_comments => false}},
            {elvis_style, no_tabs},
            {elvis_style, no_trailing_whitespace},
            {elvis_style, macro_names, #{ignore => []}},
            %% {elvis_style, macro_module_names},
            {elvis_style, operator_spaces, #{rules => [{right, ","},
                                                    {right, "++"},
                                                    {left, "++"}]}},
            %% {elvis_style, nesting_level, #{level => 3}},
            {elvis_style, god_modules,
            #{limit => 50,
            ignore => []}},
            {elvis_style, no_if_expression},
            %% {elvis_style, invalid_dynamic_call, #{ignore => []}},
            {elvis_style, used_ignored_variable},
            {elvis_style, no_behavior_info},
            {
            elvis_style,
            module_naming_convention,
            #{regex => "^[a-z]([a-z0-9]*_?)*(_SUITE)?$",
                ignore => []}
            },
            {
            elvis_style,
            function_naming_convention,
            #{regex => "^([a-z][a-z0-9]*_?)*$"}
            },
            {elvis_style, state_record_and_type},
            {elvis_style, no_spec_with_records}
            %% {elvis_style, dont_repeat_yourself, #{min_complexity => 10}}
            %% {elvis_style, no_debug_call, #{ignore => []}}
        ]},
        #{dirs => ["."],
        filter => "Makefile",
        rules => [{elvis_project, no_deps_master_erlang_mk, #{ignore => []}},
                    {elvis_project, protocol_for_deps_erlang_mk, #{ignore => []}}]
        },
        #{dirs => ["."],
        filter => "rebar.config",
        rules => [
            %% {elvis_project, no_deps_master_rebar, #{ignore => []}},
            %% {elvis_project, protocol_for_deps_rebar, #{ignore => []}}
        ]
        }
    ]
}.
